import React from 'react'
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native'
import { FontAwesome } from '@expo/vector-icons'

const { width } = Dimensions.get('screen')

export default ({ therapist, showAbout }) => (
  <View style={styles.container}>
    <Image source={therapist.photo} style={styles.photo} />
    <Text style={styles.name}>{therapist.name}</Text>
    <View style={styles.row}>
      <FontAwesome name={'star'} style={styles.star} />
      <FontAwesome name={'star'} style={styles.star} />
      <FontAwesome name={'star'} style={styles.star} />
      <FontAwesome name={'star'} style={styles.star} />
      <FontAwesome name={'star'} style={styles.star} />
    </View>

    <View style={styles.row}>
      <Text style={styles.leftText}>Years Training:</Text>
      <Text style={styles.rightText}>{therapist.yearsOfTraining}</Text>
    </View>
    {showAbout &&
      <View style={styles.row}>
        <Text style={styles.leftText}>{therapist.about}</Text>
      </View>}
  </View>
)

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    width: width,
    padding: 12
  },
  photo: {
    width: width * 0.4,
    height: null,
    aspectRatio: 1,
    borderRadius: width * 0.2
  },
  row: {
    flexDirection: 'row'
  },
  name: {
    fontSize: 20
  },
  star: {
    color: '#FEC80F',
    fontSize: 14
  },
  leftText: {
    fontSize: 12,
    margin: 2
  },
  rightText: {
    fontSize: 12,
    fontWeight: 'bold',
    margin: 2
  }
})
