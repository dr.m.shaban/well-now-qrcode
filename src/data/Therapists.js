export default {
  '123': {
    name: 'Hector M.',
    photo: require('../images/Hector.png'),
    yearsOfTraining: '15',
    about: 'Growing up I used to be a pretty shy kid. However, through training, I noticed that I became more outgoing and people noticed the work that was put in. Which in turn I became passionate about coaching other people as that would allow me to instill those same ideas.',
    qrCode: require('../images/123.png')
  },
  '456': {
    name: 'Rob S.',
    photo: require('../images/Rob.jpg'),
    yearsOfTraining: '10',
    about: 'My name is Rob and I’ve been a mover all my life. As a kid, I never could sit still and I dabbled in every sport that was accessible to me. Through my personal experiences I figured out that exercise was a tool to achieve self-growth, confidence, and discovery.',
    qrCode: require('../images/456.png')
  }
}
