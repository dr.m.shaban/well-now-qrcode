import React, { Component } from 'react'
import { View, Text, StyleSheet, Button } from 'react-native'

class SelectScreen extends Component {
  static navigationOptions = {
    title: 'Select Screen'
  }

  onCustomerPressed = () => {
    this.props.navigation.navigate('customer')
  }

  onTheapistPressed = id => () => {
    this.props.navigation.navigate('therapistDetails', {
      id
    })
  }

  render () {
    return (
      <View style={styles.container}>
        <View style={styles.button}>
          <Button
            title='Scan Therapist QR Code'
            onPress={this.onCustomerPressed}
          />
        </View>
        <View style={styles.button}>
          <Button
            title='Login as Therapist 1'
            onPress={this.onTheapistPressed('123')}
          />
        </View>
        <View style={styles.button}>
          <Button
            title='Login as Therapist 2'
            onPress={this.onTheapistPressed('456')}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  button: {
    margin: 6
  }
})

export default SelectScreen
