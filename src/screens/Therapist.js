import React, { Component } from 'react'
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native'

import TherapistCard from '../components/TherapistCard'
import Therapists from '../data/Therapists'

const { width } = Dimensions.get('screen')

class Therapist extends Component {
  static navigationOptions = {
    title: 'Therapist Details'
  }

  state = {
    therapist: Therapists[this.props.navigation.state.params.id]
  }

  render () {
    const { therapist } = this.state
    return (
      <View style={styles.container}>
        <TherapistCard therapist={therapist} showAbout />
        <Image source={therapist.qrCode} style={styles.qrcode} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    padding: 12,
    backgroundColor: 'white'
  },
  qrcode: {
    width: width * 0.5,
    height: null,
    aspectRatio: 1
  }
})

export default Therapist
