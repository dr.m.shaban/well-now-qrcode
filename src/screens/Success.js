import React, { Component } from 'react'
import { View, Text, Dimensions, Image, StyleSheet } from 'react-native'
import { DangerZone } from 'expo'
import { Entypo } from '@expo/vector-icons'
let { Lottie } = DangerZone

import TherapistCard from '../components/TherapistCard'
import Therapists from '../data/Therapists'

export default class Success extends Component {
  static navigationOptions = {
    title: 'Thank you'
  }

  state = {
    therapist: Therapists[this.props.navigation.state.params.id],
    animation: true,
    animating: false
  }

  componentDidMount = () => {
    this.animation.reset()
    this.animation.play()

    setTimeout(() => {
      this.setState({
        animating: true
      })
    }, 400)

    setTimeout(() => {
      this.setState({
        animation: false
      })
    }, 5000)
  }

  hideAnimation = () => {
    this.setState({
      animation: false
    })
  }

  render () {
    const { therapist } = this.state
    return (
      <View style={styles.container}>
        <TherapistCard therapist={therapist} />
        <Text style={styles.thankyou}> Thank you for using Wellnow </Text>
        <Entypo name='emoji-happy' style={styles.happy} />
        <Text>
          You have earned
          {' '}
          <Text style={styles.prize}>$10</Text>
          {' '}
          for yout next appointment
        </Text>
        {this.state.animation &&
          <Lottie
            ref={animation => {
              this.animation = animation
            }}
            loop={false}
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'transparent',
              position: 'absolute',
              opacity: this.state.animating ? 1 : 0
            }}
            source={require('../images/animation-w800-h600.json')}
          />}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: 'white'
  },
  thankyou: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  happy: {
    color: '#1A8A9C',
    fontSize: 100
  },
  prize: {
    fontWeight: 'bold',
    fontSize: 16
  }
})
