import React, { Component } from 'react'
import { View, Text, Dimensions, StyleSheet } from 'react-native'
import { Constants, BarCodeScanner, Permissions } from 'expo'

export default class Customer extends Component {
  static navigationOptions = {
    title: 'Scan QR Code'
  }
  state = {
    hasCameraPermission: null,
    latestCode: null
  }

  componentDidMount () {
    this._requestCameraPermission()
  }

  _requestCameraPermission = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({
      hasCameraPermission: status === 'granted'
    })
  }

  _handleBarCodeRead = ({ data }) => {
    if (this.state.latestCode !== data) {
      this.setState(
        {
          latestCode: data
        },
        () => {
          this.props.navigation.navigate('success', {
            id: data
          })
        }
      )
    }
  }

  render () {
    return (
      <View style={styles.container}>
        {this.state.hasCameraPermission === null
          ? <Text>Openining Camera</Text>
          : this.state.hasCameraPermission === false
              ? <Text>You need to allow the app to use Camera</Text>
              : <BarCodeScanner
                onBarCodeRead={this._handleBarCodeRead}
                style={{ flex: 1, width: '100%' }}
                />}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1'
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#34495e'
  }
})
