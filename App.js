import { createStackNavigator } from 'react-navigation'

import Therapist from './src/screens/Therapist'
import SelectScreen from './src/screens/SelectScreen'
import Customer from './src/screens/Customer'
import Success from './src/screens/Success'

export default createStackNavigator({
  selectScreen: {
    screen: SelectScreen
  },
  therapistDetails: {
    screen: Therapist
  },
  customer: {
    screen: Customer
  },
  success: {
    screen: Success
  }
})
